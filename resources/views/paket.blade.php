@extends('admin.layout')

@section('content')
    <main>
        <div class="container">
            <h2>Packages List</h2>
            <a href="/tambahpaket" class="btn btn-success">Tambah Paket</a>

            <table class="table table-striped" id="mytable">
            <thead>
                <tr>
                <th>Packages</th>
                <th>Deskripsi</th>
                <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datapaket as $item)
                    <tr>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->deskripsi}}</td>
                        
                        <td>
                            <a href="#"class="btn btn-danger delete" data-id={{$item->id}} data-name={{$item->nama}}>Delete</a>
                            <a href="/edit/{{$item->id}}" class="btn btn-info" >Edit</a>

                        </td>
                    </tr>
                @endforeach
                
            </tbody>
            </table>
        </div>
        <!-- <div class="alert alert-danger"> asdf </div> -->

    </main>

    <script>
        @if(Session::has('success'))
            toastr.success("{{Session::get('success')}}")
        @endif

    </script>

    <script>
        $('.delete').click(function(){
            var idpkt = $(this).attr('data-id');
            var nama = $(this).attr('data-name');
            swal({
                title: "Apakah anda yakin ingin menghapus "+nama+" ?",
                text: "Data yang sudah dihapus tidak bisa dikembalikan !!!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "/hapuspaket/"+idpkt+""
                    swal("Data Terhapus !!!", {
                    icon: "success",
                    });
                } else {
                    swal("Data Tidak Jadi dihapus");
                }
            });
        })
        
    </script>
@endsection
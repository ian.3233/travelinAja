<?php

namespace App\Http\Controllers;

use App\Models\pesan;
use Illuminate\Http\Request;

class PesanController extends Controller
{
    //
    public function pushPesan(Request $request){
        // dd($request);
        $pesan = new pesan();
        $pesan->nama = $request->nama;
        $pesan->email = $request->email;
        $pesan->subject = $request->subject;
        $pesan->message = $request->message;
        $pesan->save();
        return redirect('/')->with('success','Data Berhasil Dihapus');

        // return view('index')->with('success','Pesan berhasil terkirim');
    }

    public function hapusPesan($id){
        
        $inbox = pesan::find($id);
        $inbox->delete();
        return redirect('/inbox')->with('success','Data Berhasil Dihapus');

        // return redirect('/inbox')->with('success','Data Berhasil Dihapus');
    }

    public function inbox(){
        $inbox = pesan::all();
        // dd($data);
        return view('pesan',compact('inbox'));
    }
}

@extends('admin.layout')

@section('content')
    <div class="container">
        <h2>Mail List</h2>
        <table class="table table-striped" id="mytable">
        <thead>
            <tr>
            <th>Nama</th>
            <th>Email</th>
            <th>Subject</th>
            <th>Pesan</th>
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($inbox as $item)
                <tr>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->subject}}</td>
                    <td>{{$item->message}}</td>
                    <td>
                        <a href="#"class="btn btn-danger delete" data-id={{$item->id}}>Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        </table>
    </div>

    <script>
        @if(Session::has('success'))
            toastr.error("{{Session::get('success')}}")
        @endif

    </script>

    <script>
        $('.delete').click(function(){
            var idpsn = $(this).attr('data-id');
            swal({
                title: "Apakah anda yakin ingin menghapus  ?",
                text: "Data yang sudah dihapus tidak bisa dikembalikan !!!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "/hapuspesan/"+idpsn+"" 
                    // swal("Data Terhapus !!!", {
                    // icon: "success",
                    // });
                } else {
                    swal("Data Tidak Jadi dihapus");
                }
            });
        })
        
    </script>
@endsection
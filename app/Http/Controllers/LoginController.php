<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    public function login(){
        return view('login');
    }

    public function loginProses(Request $request){
        // $nama = $request->name;

        if(Auth::attempt($request->only('name','password'))){
            // return redirect('/admin');
            return redirect('/admin')->with('success',"Login Sukses Selamat Datang");
        }
        return \redirect('/login')->with('error',"Username Atau Password Salah !!!");
    }

    public function logout(){
        Auth::logout();
        return redirect('/login')->with('success',"Berhasil Logout");
    }
}

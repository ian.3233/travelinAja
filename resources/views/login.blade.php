<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Login Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<link rel="stylesheet" href="/assets/css/login.css" type="text/css"/>
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	
	</head>

	<body>
		<div class="login-form align-items-center flex pt-5">
			<h1>Login Form</h1>

			<form action="/loginproses" method="POST">
				@csrf
				<input type="text" name="name" placeholder="Username" required>
				<input type="password" name="password" placeholder="Password" required>
				<button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
			</form>
		</div>
	</body>

	<script>
        @if(Session::has('error'))
            toastr.error("{{Session::get('error')}}")
        @endif
    </script>

</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TravelinAja Admin Panel</title>

    <!-- CSS (load bootstrap from a CDN) -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <style>
        body { padding-top:50px; }
    </style>
</head>


<body class="container">
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="/admin">TravelinAja Admin Panel</a>
            <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="inbox">Inbox</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="paket">Packages</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="about">About</a>
            </li>
            <li>
                <a class="nav-link" href="logout">Logout</a>
{{-- 
                <form action="/logout" method="post">
                    @csrf
                    <button type="submit" class="btn btn-link navbar-btn navbar-link logout "> Log off</button>
                </form> --}}
            </li>
            </ul>
        </nav>
    </header>

    @yield('content')

    <footer>
        <p class="text-center text-muted">© Copyright TravelinAja. All Rights Reserved</p>
    </footer>

</body>

<script>
    $('.logout').click(function(){
        swal({
            title: "Apakah anda yakin ingin Logout?",
            text: "Anda Harus login jika mengakses halaman admin",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                window.location = "/logout"
                swal("Logout Sukses !!!", {
                icon: "success",
                });
            } else {
                swal("Tidak Jadi Logout");
            }
        });
    })
    
</script>

</html>
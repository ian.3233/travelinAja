<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'superadmin@gmail.com',
                'password' => bcrypt('admin'),
              ],
              [
                'name' => 'ian',
                'email' => 'accountadmin@gmail.com',
                'password' => bcrypt('ian'),
              ]
        ]);
    }
}
